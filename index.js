// console.log("Hello world");

// Part 1

let number = Number(window.prompt('Enter a number: '));
	console.log(`The number you provided is ${number} .`);

for(let i = number; i >= 0; i--){
	if(i <= 50){
		console.log(`The current value is at ${i} Terminating the loop`);
		break;
	}
	else if(i % 10 === 0){
		console.log(`The number is divisible by 10. Skipping the number.`);
		continue;
	}
	else if(i % 5 === 0){
		console.log(i);
	}
};

// Part 2

let word = 'supercalifragilisticexpialidocious';
console.log('supercalifragilisticexpialidocious');

let consonants = '';

for(let i = 0; i < word.length; i++){
	if(
		word[i].toLowerCase() == 'a' ||
		word[i].toLowerCase() == 'e' ||
		word[i].toLowerCase() == 'i' ||
		word[i].toLowerCase() == 'o' ||
		word[i].toLowerCase() == 'u'
	){
		continue;
	}
		consonants += word[i];
};
console.log(consonants);











